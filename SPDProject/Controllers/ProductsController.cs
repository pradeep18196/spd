﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
namespace SPDProject.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        private SPDEntities ctx;
        public ProductsController()
        {
            ctx = new SPDEntities();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddProducts()
        {
            DropDowns();
            return View();
        }
        [NonAction]
        public void DropDowns()
        {
            ViewBag.Colors = ctx.Params.Where(x => x.Types == "Colors").Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.ParamId.ToString()
            }).ToList();

            ViewBag.Categories = ctx.Params.Where(x => x.Types == "Categories").Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.ParamId.ToString()
            }).ToList();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveProducts(Product product,HttpPostedFileBase File)
        {
            using (ctx)
            {
                try
                {
                    if (ModelState.IsValid) {
                       
                        product.ProductImage = File.FileName;
                        ctx.Products.Add(product);
                        ctx.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return Redirect("Index");
            };
        }
    }
}